<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\{Str,Facades\Hash};
use Illuminate\Support\Facades\Validator;
use App\User;
/*
    |--------------------------------------------------------------------------
    | ENABLE CORS
    |--------------------------------------------------------------------------
    |
    | To enable CORS origin added to bootstrap/app.php
    | header('Access-Control-Allow-Origin: *');
    | header('Access-Control-Allow-Methods: *');
    | header('Access-Control-Allow-Headers: *');
    |
*/
class RegisterController extends Controller
{   
    private $_token;

    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct() {
        // Unique Token
        $this->_token = uniqid(base64_encode(Str::random(20)));
        $this->middleware('guest');
    }

    public function create(Request $request){
        $rules = [
            'password' => 'required|confirmed'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()):
            return response()->json([
                'message' => $validator->messages(),
            ]);
        else:
            $requestData = [
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => Hash::make($request->password),
                'token'     => $this->_token
            ];

            if($user = User::create($requestData)):
                return response()->json($user);
            else:
                return response()->json([
                    'message' => 'Registration failed, please try again.',
                ]);
            endif;
        endif;
    }
}