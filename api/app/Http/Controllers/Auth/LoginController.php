<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\{Str,Facades\Validator};
use App\User;
/*
    |--------------------------------------------------------------------------
    | ENABLE CORS
    |--------------------------------------------------------------------------
    |
    | To enable CORS origin added to bootstrap/app.php
    | header('Access-Control-Allow-Origin: *');
    | header('Access-Control-Allow-Methods: *');
    | header('Access-Control-Allow-Headers: *');
    |
*/
class LoginController extends Controller
{
    private $_token;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        // Unique Token
        $this->_token = uniqid(base64_encode(Str::random(20)));
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request){
        // Validations
        $rules = [
            'email'=>'required|email',
            'password'=>'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) :
            // Validation failed
            return response()->json([
                'message' => $validator->messages(),
            ]);
        else :
          // GET User
          $user = User::where('email',$request->email)->first();
          if($user):
                // Verify the password
                if(password_verify($request->password, $user->password)):
                  // Update Token
                    $tokenRequest = ['token' => $this->_token];
                    if($login = User::where('email',$request->email)->update($tokenRequest)):
                        $user->token  = $this->_token;
                        return response()->json($user);
                    endif;
                else:
                    return response()->json([
                        'message' => 'Invalid Password',
                    ]);
                endif;
            else :
                return response()->json([
                    'message' => 'User not found',
                ]);
            endif;
        endif;

    }
}
